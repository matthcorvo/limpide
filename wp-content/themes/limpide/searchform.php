<form class="d-flex" action="<?php echo home_url('/'); ?>" method="get">
        <input class="search-input form-control" type="search" placeholder="Rechercher..." value="<?php echo the_search_query(); ?>" name="s">
        <button class="search-btn" type="submit"><i class="fas fa-search"></i></button>
      </form>