<?php


 //WP_customize_control(changer background img banner hero) 
 function hero_customizer_settings($wp_customizer){

    //add a customizer setting to the
    //include new section
    $wp_customizer->add_section('header', array(
        'title'=>__('Header Setting','Daily'),
        'priority'=>70
    ));

    // capability
    $wp_customizer->add_setting('header_image',array(
        'capability'=>'edit_theme_options'
    ));

    // controle
    $wp_customizer->add_control(new WP_customize_image_Control($wp_customizer,'header_image',array(
    'label'=>__('Header image','Daily'),
    'section'=> 'header'
)));



// adding header text
$wp_customizer->add_setting('header_text_field',array(
    'capability'=>'edit_theme_options'
));

// adding text control
$wp_customizer->add_control('header_text_control',array(
    'label' =>__('Header Text','Daily'),
    'description' =>'Change Header text',
    'section' =>'header',
    'settings' => 'header_text_field'
    ));

 }

 add_action('customize_register','hero_customizer_settings')




?>