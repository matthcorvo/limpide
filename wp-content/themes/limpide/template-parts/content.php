<?php if (have_posts()) :  while (have_posts()) : the_post();?>

 <div class="position-relative entry-banner" >
		<div class="container d-flex justify-content-center">
			<div class="entry-banner-content">
<h1 class="entry-title"><?php echo get_the_title(); ?></h1>
		</div>
	</div>

</div>

<div class="content-area">

<div class="container">
<div class="row mb-4">
				<ul class="entry-meta mb-3">
			<li class="single-meta"><i class="fa fa-calendar" ></i><span><?php echo the_date();?> </span></li>
			<li class="single-meta"><i class="fa fa-user" ></i>Cuisiné par <?php the_author(); ?> </li>
				
		<li class="single-meta "><i class="fa fa-tags" ></i>
         <?php $categories = get_the_category();  
            foreach($categories as $cat) : ?>
                 <a href="<?php echo get_tag_link($cat->term_id); ?>">   <?php echo $cat->name ?></a>
               <?php endforeach;?>
    </li>
		
        
        <li class="single-meta"><i class="fa fa-book" ></i>Cuisine: <span >
            <?php $tags = get_the_tags(); 
            foreach($tags as $tag) : ?>
            
            <a class="badge bg-secondary" href="<?php echo get_tag_link($tag->term_id); ?>"><?php echo $tag->name ?></a>
            
            <?php endforeach;?></span></li></ul>
			</div>
			
            <div class="d-flex justify-content-center">
            <?php if(has_post_thumbnail()):?>
                <img src="<?php the_post_thumbnail_url();?>" alt="<?php the_title();?>" class="img-fluid mb-3 img-thumbnail">
               </div>
                <?php endif;?>
                
                <div class="item-feature">
                    <?php if( have_rows('media_info') ): ?>
                    <ul>
                    <?php while( have_rows('media_info') ) : the_row();

$image = get_sub_field('icone');
$title = get_sub_field('title');
$nbr = get_sub_field('chiffre');
$time = get_sub_field('time');
$text = get_sub_field('text');

?>
								<li class="active-4">
					<div class="feature-wrap">
						<div class="media">
							<div class="feature-icon">
                            <img src="<?php echo ($image['url']); ?>" alt="img">    
                            </div>
							<div class="media-body space-sm">
								<div class="feature-title"><?php echo $title; ?></div>
								<div class="feature-sub-title"><?php echo $nbr; ?> <?php echo $time; ?> <?php echo $text; ?></div>
							</div>
						</div>
					</div>
				</li>
                <?php endwhile; ?>

							</ul>
                            <?php endif; ?>

		</div>
            <?php the_content(); ?>
            
            <div class="d-flex justify-content-center a">
            <ul class="tabs " role="tablist">
    <li>
        <input type="radio" name="tabs" id="tab1" checked />
        <label for="tab1" 
               class="tabslabel"
        role="tab" 
               aria-selected="true" 
               aria-controls="panel1" 
               tabindex="0"><i class="fas fa-list"></i>&ensp; Ingrédient</label>
        <div id="tab-content1" 
             class="tab-content" 
             role="tabpanel" 
             aria-labelledby="description" 
             aria-hidden="false">
             <?php if( have_rows('recipes') ): ?>


                <?php while( have_rows('recipes') ) : the_row();

$text = get_sub_field('ingredients');

?><div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
  <label class="form-check-label" for="flexCheckDefault">
  
  <p><?php echo $text; ?></p>
  </label></div>
  <?php endwhile; ?>


<?php endif; ?>
            </li>
 

    <li>
        <input type="radio" name="tabs" id="tab2" />
        <label for="tab2"
               role="tab" 
               class="tabslabel"
               aria-selected="false" 
               aria-controls="panel2" 
               tabindex="0">Directions</label>
        <div id="tab-content3" 
             class="tab-content"
             role="tabpanel" 
             aria-labelledby="specification" 
             aria-hidden="true">
          <h4>A VOS FOURNEAUX !</h4>
          <?php if( have_rows('recipes_etapes') ): ?>

        

          <?php while( have_rows('recipes_etapes') ) : the_row();

$nbr = get_sub_field('etapes');
$text = get_sub_field('recipes_desc');

?>  <div class="pt-4">
                  <div class=" btn btn-success btn-sm">
                       <h3><b>Etape <?php echo $nbr; ?></b></h3> 
                    </div>
                   
                  <p class="serial-desc"><?php echo $text; ?></p> </div>
                  <?php endwhile; ?>
          
           <?php endif; ?>


    </li>

</ul>
</div>
<br style="clear: both;" />
<br style="clear: both;" />
<br style="clear: both;" />

		
<div class="mb-5"> </div>
		</div>

</div>

<?php endwhile;  else : endif ?>
