
<header >
   
   <nav class="navbar navbar-expand-lg navbar-light bg-light" aria-label="Tenth navbar example">
       <div class="container-fluid">
       <a class="navbar-brand" href="#">
       <?php
       if(function_exists('the_custom_logo')){
           the_custom_logo();
       }else{
           bloginfo('name');
       }
       ?>
       </a>
         <div class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
           Menu <span class="navbar-toggler-icon"></span>
         </div>
   
         <div class="collapse navbar-collapse justify-content-md-center" id="navbarsExample08">
         <?php
            wp_nav_menu(array(
                'theme_location' => 'top-menu',
                'depth'             => 2,
                'container'         => 'ul',
                'container_class'   => 'collapse navbar-collapse',
                'container_id'      => 'bs-example-navbar-collapse-1',
                'menu_class'        => 'nav navbar-nav',
                'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                'walker'            => new WP_Bootstrap_Navwalker(),
            ) );
            ?>
         
         </div>
       </div>
     </nav>
       </header>   
      