
<?php if (have_posts()) :  while (have_posts()) : the_post();?>

<div class="d-flex">
<div class="row">
<div class="col-lg-11 d-flex">
<div class="card mt-5">
<a href="<?php the_permalink(); ?>">
       <?php if(has_post_thumbnail()) : ?>
        <img  src="<?php  the_post_thumbnail_url('blog-small'); ?>" class="card-img img-thumbnail" alt="Bologna">

       <?php endif; ?>
       
        <div class="card-img-overlay">

        <?php $categories = get_the_category(); 
            foreach($categories as $cat) : ?>
          <button href="" class="btn btn-light btn-sm"><?php echo $cat->name ?></button>
          <?php endforeach;?>
        </div>

        <?php $tags = get_the_tags(); 
                 foreach($tags as $tag) : ?>
        <button  class="btn btn-light btn-sm w-100">
          <b><?php echo $tag->name ?></b>
        </button>
        <?php endforeach;?>

        <div class="card-body text-center">
          <h4 class="card-title"><?php echo the_title(); ?></h4>
          <small class="text-muted cat">
            
            <i class="far fa-clock text-info"></i> 30 minutes
            <i class="fas fa-users text-info"></i> 4 portions
          </small>
          <div class="card-footer text-muted d-flex justify-content-between bg-transparent border-top-0">
          <div class="views"><?php echo get_the_date('d/m/Y'); ?></div>
          <div class="stats">
           	<i class="far fa-user"></i> <?php the_author(); ?>
          </div>
           
        </div>
        </a>
      </div>
      </div>
      </div>
</div>
<?php endwhile;  else : endif ?>

