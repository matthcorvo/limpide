<?php

/**
 * Setup theme
 *
 * @return void
 */
function limpide_setup() : void
{
    // Add theme supports
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
    add_theme_support('menus');

      //Ajout de fonctionnalités
      add_theme_support('html5',array('search-form'));
      add_theme_support('custom-logo');
      
    // Register menus
    register_nav_menus([
        'primary-menu' => _x('Primary menu', 'Menu name', 'limpide'),
    ]);
}
add_action('after_setup_theme', 'limpide_setup');

function add_css()
{
    wp_register_style('font', get_template_directory_uri() . '/assets/font/fontawesome/css/all.css', array(), 1,'all');
    wp_enqueue_style('font');

    wp_register_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.css', array(), 1,'all');
    wp_enqueue_style('bootstrap');
    
    wp_register_style('main', get_template_directory_uri() . '/assets/css/main.css', array(), 1,'all');
    wp_enqueue_style('main');
}
add_action('wp_enqueue_scripts','add_css');


// Scriptss

// include custom jQuery
function add_js() {

    wp_register_script('bootstrapjs', get_template_directory_uri() . '/assets/js/bootstrap.bundle.js', array( 'popper_js' ), '1.0.0', true  );
    wp_enqueue_script('bootstrapjs');

}
add_action('wp_enqueue_scripts', 'add_js');

// menu support
add_theme_support('menus');

// register menu
register_nav_menus(

    array(
        'top-menu' => __('Top Menu','theme'),
    )
    );


    //  image size 
    add_image_size('blog-small', 150, 150, true);


    // sidebars
    function my_sidebars(){
        register_sidebar(
            array(
                'name' => 'Page Sidebar',
                'id' => 'page-sidebar',
                'befor-title'=>'<h4 class="widget-title>',
                'after_title' => '</h4>'
            )
            );
    }
    add_action('widgets_init','my_sidebars');

     // sidebars
     function my_Blogsidebars(){
        register_sidebar(
            array(
                'name' => 'Blog Sidebar',
                'id' => 'Blog-sidebar',
                'befor-title'=>'<h4 class="widget-title>',
                'after_title' => '</h4>'
            )
            );
    }
    add_action('widgets_init','my_Blogsidebars');


// Numbered Pagination
if ( !function_exists( 'wpex_pagination' ) ) {
	
	function wpex_pagination() {
		
		$prev_arrow = is_rtl() ? '→' : '←';
		$next_arrow = is_rtl() ? '←' : '→';
		
		global $wp_query;
		$total = $wp_query->max_num_pages;
		$big = 999999999; // need an unlikely integer
		if( $total > 1 )  {
			 if( !$current_page = get_query_var('paged') )
				 $current_page = 1;
			 if( get_option('permalink_structure') ) {
				 $format = 'page/%#%/';
			 } else {
				 $format = '&paged=%#%';
			 }
			echo paginate_links(array(
				'base'			=> str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format'		=> $format,
				'current'		=> max( 1, get_query_var('paged') ),
				'total' 		=> $total,
				'mid_size'		=> 3,
				'type' 			=> 'list',
				'prev_text'		=> $prev_arrow,
				'next_text'		=> $next_arrow,
			 ) );
		}
	}
	
}


/**
 * Register Custom Navigation Walker
 */

add_filter( 'nav_menu_link_attributes', 'prefix_bs5_dropdown_data_attribute', 20, 3 );
/**
 * Use namespaced data attribute for Bootstrap's dropdown toggles.
 *
 * @param array    $atts HTML attributes applied to the item's `<a>` element.
 * @param WP_Post  $item The current menu item.
 * @param stdClass $args An object of wp_nav_menu() arguments.
 * @return array
 */
function prefix_bs5_dropdown_data_attribute( $atts, $item, $args ) {
    if ( is_a( $args->walker, 'WP_Bootstrap_Navwalker' ) ) {
        if ( array_key_exists( 'data-toggle', $atts ) ) {
            unset( $atts['data-toggle'] );
            $atts['data-bs-toggle'] = 'dropdown';
        }
    }
    return $atts;
}
function register_navwalker(){
	require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
}
add_action( 'after_setup_theme', 'register_navwalker' );
    // add customizer.php file 
require_once('template-parts/helpers/customizer.php');

