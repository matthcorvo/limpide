<?php
get_header(); ?>

<section >
<div class="container">

<div class="position-relative cat-banner" >
		<div class="container d-flex justify-content-center">
			<div class="entry-banner-cat">
            <h1> résultat de la recherche pour '<?php echo get_search_query();?>' </h1>
		</div>
	</div>

</div>


        <?php get_template_part(  'template-parts/content','card' ); ?> 

        
        
    </div>
</section>
<div class="container mt-3 d-flex justify-content-center">

<?php
                global $wp_query;
                $big = 999999999; // need an unlikely integer
                $links = paginate_links( array(
                    'base'      => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
                    'format'    => '?paged=%#%',
                    'current'   => max( 1, get_query_var('paged') ),
                    'total'     => $wp_query->max_num_pages,
                    'type'      => 'array',
                ));
            ?>
            <?php if (count($links) > 0) : ?>
            <div class="archive-navigation">
                <?php foreach ($links as $link) : ?>
                <?php echo $link; ?>
                <?php endforeach; ?>
            </div>
            <?php endif ?><!-- End of blog-pagination -->
</div>

<?php
get_footer();?>