<?php

get_header(); ?>
<div class="hero" 
   <?php if(get_theme_mod('header_image')) : ?>
   style="background-image:url( <?php echo esc_url(get_theme_mod('header_image'))?>)" <?php endif;?>>
  
<div class="overlay"></div>
  <div class="content">
    
    <?php if(get_theme_mod('header_text_field')) : ?>
  <h1> <?php echo get_theme_mod('header_text_field')?></h1>
   <?php endif;?>
         <div class="search-box pt-4">
             <?php get_search_form(); ?>
  
</div>
</div>
</div>


<div class="container">
      
<section class="row">
      <div class="col-lg-3  ">

        <?php if( is_active_sidebar('page-sidebar')) : ?>
        
         <?php dynamic_sidebar('page-sidebar'); ?>
        
        <?php endif; ?>

      </div>

<div class="col-lg-9">
<?php get_template_part('template-parts/content','card'); ?> 

      </div>

</section>

</div>
<div class="container mt-3 d-flex justify-content-center">

<?php wpex_pagination(); ?>           
</div>

<?php
get_footer();?>