<?php
get_header(); ?>

<section >
<div class="container">

<div class="position-relative cat-banner" >
		<div class="container d-flex justify-content-center">
			<div class="entry-banner-cat">
            <h1> <?php echo single_cat_title();?> </h1>
		</div>
	</div>

</div>


        <?php get_template_part('template-parts/content','card'); ?> 

        
        
    </div>
</section>
<div class="container mt-3 d-flex justify-content-center">


<?php the_posts_pagination(array('type' =>'list')); ?>

           
</div>

<?php
get_footer();?>