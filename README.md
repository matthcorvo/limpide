# Limpide test dev

## Installation

1. Cloner le repo
2. Créer une base de données et lancer l'installation de Wordpress
3. Travailler dans le dossier du thème **limpide**

## Exercice

> Le client souhaite pouvoir proposer des recettes sur son site.  
> Ces recettes doivent pouvoir êtres catégorisées ainsi qu'étiquetées.  
> Les articles sont déjà utilisés pour afficher des articles de blog avec Gutenberg.  
> Il faut prévoir une page de listing des recettes avec pagination et nombre d'éléments affichés par page administrable.  
> Prévoir également une page de listing par catégorie et par étiquette.  
> Cette page de listing devra afficher l'image de la recette, son titre, et sa durée de réalisation.  
> Les recettes doivent avoir une image, un titre, une liste d'ingrédients et une durée de réalisation. Le contenu de celle-ci reste un contenu libre comme pour la rédaction d'un article ou d'une page.  
> Prévoir un système d'onglet pour switcher entre la liste des ingrédients et le contenu de la recette sur la page de celle-ci.

> Libre à vous d'utiliser les outils que vous souhaitez pour réaliser cette demande.  
> Nous vous mettons à disposition le plugin ACF si besoin.

## Rendu

- Dump SQL
- Dossier du thème "wp-content/themes/limpide"
- Login / Mdp de l'administration
- Tout autre élément qui vous semblerait pertinent de nous partager

> Il n'est pas nécessaire dans le cadre de cet exercice de remplir les recettes avec du vrai contenu.
